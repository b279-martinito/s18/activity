/*
	
	1. Create a function called addNum which will be able to add two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the addition.
	   
	   Create a function called subNum which will be able to subtract two numbers.
	    - Numbers must be provided as arguments.
	    - Return the result of subtraction.

	    Create a new variable called sum.
	     - This sum variable should be able to receive and store the result of addNum function.

	    Create a new variable called difference.
	     - This difference variable should be able to receive and store the result of subNum function.

	    Log the value of sum variable in the console.
	    Log the value of difference variable in the console.


	2. Create a function called multiplyNum which will be able to multiply two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the multiplication.

		Create a function divideNum which will be able to divide two numbers.
		- Numbers must be provided as arguments.
		- Return the result of the division.

		Create a new variable called product.
		 - This product variable should be able to receive and store the result of multiplyNum function.

		Create a new variable called quotient.
		 - This quotient variable should be able to receive and store the result of divideNum function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.


	3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
		- a number should be provided as an argument.
		- look up the formula for calculating the area of a circle with a provided/given radius.
		- look up the use of the exponent operator.
		- return the result of the area calculation.

		Create a new variable called circleArea.
		- This variable should be able to receive and store the result of the circle area calculation.
		- Log the value of the circleArea variable in the console.

	4. Create a function called getAverage which will be able to get total average of four numbers.
		- 4 numbers should be provided as an argument.
		- look up the formula for calculating the average of numbers.
		- return the result of the average calculation.
		
		Create a new variable called averageVar.
		- This variable should be able to receive and store the result of the average calculation
		- Log the value of the averageVar variable in the console.
	

	5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
		- this function should take 2 numbers as an argument, your score and the total score.
		- First, get the percentage of your score against the total. You can look up the formula to get percentage.
		- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
		- return the value of the variable isPassed.
		- This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

function addNum(num1 = 5, num2 = 15){
	let sum = num1 + num2;
	console.log("Displayed sum of 5 and 15");
	return sum;
}

let addition = addNum();
console.log(addition);


function subNum(sub1 = 20, sub2 = 5){
	let difference = sub1 - sub2;
	console.log("Displayed difference of 20 and 5");
	return difference;
}

let subtraction = subNum();
console.log(subtraction);



function multiplyNum(multNum1 = 50, multNum2 = 10){
	let product = multNum1 * multNum2;
	console.log("The product of 50 and 10:");
	return product;
}

let multiplication = multiplyNum();
console.log(multiplication);

function divideNum(divNum1 = 50, divNum2 = 10 ){
	let quotient = divNum1 / divNum2;
	console.log("The quotient of 50 and 10:");
	return quotient;
}

let division = divideNum();
console.log(division);


function getCircleArea(radius = 15){
	let squared = radius ** 2;
	let circleArea = 3.14159 * squared;
	console.log("The result of getting the area of a circle with 15 radius:");
	return circleArea;
}

let circleArea = getCircleArea().toFixed(2);
console.log(circleArea);


function getAverage(sumNum1 = 20, sumNum2 = 40, sumNum3 = 60, sumNum4 = 80){
	let averageVar = (sumNum1 + sumNum2 + sumNum3 + sumNum4) / 4;
	console.log("The average of 20,40,60 and 80:");
	return averageVar;
}

let totalAverage = getAverage();
console.log(totalAverage);


function checkIfPassed(earnedPoints = 38, totalPoints = 50) {
  let scorePercentage = (earnedPoints / totalPoints) * 100;
  let isPassed = scorePercentage > 75;
  console.log("Is 38/50 a passing score?")
  return isPassed;
}


let isPassingScore = checkIfPassed();
console.log(isPassingScore);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}